# Test REST API application "Learning Management System"
## Deploy steps
1) Upload database dump from demo/dump.sql
2) Add rewrite rules to nginx config:

location ~ ^/(\w+/)?(\w+/)?api/ {
    rewrite ^/(\w+/)?(\w+/)?api/(.*)$ /api.php?_d=$3&ajax_custom=1&$args last;
    rewrite_log off;
}

3) Specify paths and database data in config.php file

## Logins and passwords

Login and password are the same for user. 
For ex: 
Login: sandstorm73
Pass:  sandstorm73

## Authors and acknowledgment
Developer: Andrew Ovchenkov
