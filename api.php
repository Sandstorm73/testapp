<?php

define('BOOTSTRAP', true);

use TestApp\Core\App;

$config = require_once('config.php');

try {
    $app = new App($config);

    $app->run();
} catch (\Exception $e) {
    print_r($e);
}
