<?php

defined('BOOTSTRAP') or die('Access denied');

spl_autoload_register(function ($class) {

    if (strpos($class, 'TestApp\\') !== false) {
        $class = str_replace('TestApp\\', '', $class);
        $class = str_replace('\\', '/', $class);
    }

    include $class . '.php';
});

define('HTTP_OK', 200);
define('HTTP_CREATED', 201);
define('HTTP_BAD_REQUEST', 400);
define('HTTP_UNAUTHORIZED', 401);
define('HTTP_FORBIDDEN', 403);
define('HTTP_NOT_FOUND', 404);
define('HTTP_INTERNAL_ERROR', 500);

return [
    'app_path' => '',
    'api_path' => '/api',
    'db_server' => 'localhost',
    'db_name' => 'test_app',
    'db_user' => 'root',
    'db_password' => '----------'
];
