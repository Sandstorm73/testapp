<?php

namespace TestApp\Core;

defined('BOOTSTRAP') or die('Access denied');

/**
 * Main application class
 */
class App
{
    private $request;
    private $headers;
    private $uri;

    /**
     * Application constructor
     *
     * @param array $config Application configuration
     */
    public function __construct($config)
    {
        Database::init($config);
        $this->request = $_REQUEST;
        $this->headers = getallheaders();
        $this->uri = str_replace(
            $config['app_path'] . $config['api_path'] . '/',
            '',
            $_SERVER['REQUEST_URI']
        );
    }

    /**
     * Runs the application
     *
     * @return void
     */
    public function run()
    {
        $uri_parameters = explode('?', $this->uri);
        $uri_resource = explode('/', $uri_parameters[0]);
        $resource_name = $uri_resource[0] ?? '';
        $resource_id = $uri_resource[1] ?? '';

        $request_body = file_get_contents('php://input');
        $params = json_decode($request_body, true) ?? [];
        $params = array_merge($params, $this->request);

        if (!empty($resource_name)) {
            $resource_class = 'TestApp\\Core\\Resources\\'. ucfirst(strtolower($resource_name));

            $resource_method = '';
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                $resource_method = 'get';
            } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $resource_method = 'create';
            } elseif ($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $resource_method = 'update';
            } elseif ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
                $resource_method = 'delete';
            }

            if (class_exists($resource_class, true) && !empty($resource_method)) {
                if ($resource_class::shouldBeAuthorized() && !$this->isAuthorizedUser($this->headers)) {
                    $response = new Response(HTTP_FORBIDDEN, 'Unauthorized access');
                } else {
                    $response = $resource_class::$resource_method($resource_id, $params);
                }
            } else {
                $response = new Response(HTTP_NOT_FOUND, 'Resource does not exist');
            }

            $response_code = $response->getCode();
            http_response_code($response_code ?: 404);

            if ($response_code > 300) {
                echo(json_encode($response->getMessage()));
            } else {
                echo(json_encode($response->getData() ?? ''));
            }
        }
    }

    /**
     * Application destructor
     *
     * @param array $headers Request headers
     *
     * @return bool
     */
    private function isAuthorizedUser($headers)
    {
        $result = false;
        if (!empty($headers['Authorization']) && strpos($headers['Authorization'], 'Bearer ') === 0) {
            $key = str_replace('Bearer ', '', $headers['Authorization']);

            $db_key = Database::query(
                'SELECT `key` FROM api_users WHERE `key` = ?',
                $key
            );

            $result = !empty($db_key);
        }

        return $result;
    }

    /**
     * Application destructor
     */
    public function __destruct()
    {
        Database::close();
    }
}
