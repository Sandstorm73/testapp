<?php

namespace TestApp\Core;

defined('BOOTSTRAP') or die('Access denied');

/**
 * Database class
 */
class Database
{
    private static $db_socket = null;

    /**
     * Initiates database connection
     *
     * @param array $config Application configuration
     *
     * @return void
     */
    public static function init($config)
    {
        if (!empty($config['db_server']) &&
            !empty($config['db_name']) &&
            !empty($config['db_user']) &&
            !empty($config['db_password'])
        ) {
            self::$db_socket = new \mysqli(
                $config['db_server'],
                $config['db_user'],
                $config['db_password'],
                $config['db_name']
            );

            if (self::$db_socket->connect_errno) {
                throw new \Exception('Failed to connect to MySQL: ' . self::$db_socket->connect_error);
            }
        } else {
            throw new \Exception('No database connection');
        }
    }

    /**
     * Initiates a query to database
     *
     * @param array $sql       Body of SQL request
     * @param array ...$params List of SQL parameters
     *
     * @return array
     */
    public static function query($sql, ...$params)
    {
        $query = self::$db_socket->prepare($sql);

        if (self::$db_socket->errno) {
            throw new \Exception('MySQL error: ' . self::$db_socket->error);
        }

        $parameters_string = '';
        foreach ($params as $param) {
            if (gettype($param) === 'integer') {
                $parameters_string .= 'i';
            } elseif (gettype($param) === 'double') {
                $parameters_string .= 'd';
            } elseif (gettype($param) === 'boolean') {
                $parameters_string .= 'i';
            } else {
                $parameters_string .= 's';
            }
        }

        if (!empty($parameters_string)) {
            $query->bind_param($parameters_string, ...$params);
        }

        $query_result = [];
        $query->execute();

        if ($query->errno) {
            throw new \Exception('MySQL error: ' . $query->error);
        } else {
            $result = $query->get_result();

            if ((bool) $result) {
                while ($row = $result->fetch_assoc()) {
                    $query_result[] = $row;
                }
            }
        }

        return $query_result;
    }

    /**
     * Returns query result as an array
     *
     * @param array $sql       Body of SQL request
     * @param array ...$params List of SQL parameters
     *
     * @return array
     */
    public static function getArray($sql, ...$params)
    {
        return self::query($sql, ...$params);
    }

    /**
     * Returns query result as a row
     *
     * @param array $sql       Body of SQL request
     * @param array ...$params List of SQL parameters
     *
     * @return array
     */
    public static function getRow($sql, ...$params)
    {
        $array = self::query($sql, ...$params);
        return reset($array);
    }

    /**
     * Returns query result as a field
     *
     * @param array $sql       Body of SQL request
     * @param array ...$params List of SQL parameters
     *
     * @return array
     */
    public static function getField($sql, ...$params)
    {
        $array = self::query($sql, ...$params);
        $row = reset($array);
        return reset($row);
    }

    /**
     *  Destroys database connection when the script run has been finished.
     *
     * @return void
     */
    public static function close()
    {
        self::$db_socket->close();
    }
}
