<?php

namespace TestApp\Core\Interfaces;

defined('BOOTSTRAP') or die('Access denied');

/**
 * Interface of API resource
 */
interface IResource
{
    /**
     * Get resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function get($id = '', $params = []);

    /**
     * Create resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function create($id = '', $params = []);

    /**
     * Update resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function update($id = '', $params = []);

    /**
     * Delete resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function delete($id = '', $params = []);

    /**
     * Check if user should be authorized to use this resource
     *
     * @return bool
     */
    public static function shouldBeAuthorized();
}
