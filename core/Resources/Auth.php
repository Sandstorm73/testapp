<?php

namespace TestApp\Core\Resources;

use TestApp\Core\Interfaces\IResource;
use TestApp\Core\Database;
use TestApp\Core\Response;

defined('BOOTSTRAP') or die('Access denied');

class Auth implements IResource
{
    /**
     * Get resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function get($id = '', $params = [])
    {
        return new Response(HTTP_NOT_FOUND, 'Method is not supported');
    }

    /**
     * Create resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function create($id = '', $params = [])
    {
        if (empty($params['username']) || empty($params['password'])) {
            return new Response(HTTP_BAD_REQUEST, 'No username or password');
        }

        $username = $params['username'];
        $password = hash('sha256', $params['password']);

        $user = Database::getRow(
            'SELECT * FROM students WHERE username = ? AND password = ?',
            $username,
            $password
        );

        if (!empty($user)) {
            $api_user = Database::getRow(
                'SELECT * FROM api_users WHERE user_id = ?',
                $user['user_id']
            );

            if (!empty($api_user)) {
                $key = $api_user['key'];
            } else {
                $key = hash('sha256', $user['user_id'] . time());
                Database::query(
                    'INSERT INTO api_users (`user_id`, `key`) VALUES (?, ?)',
                    $user['user_id'],
                    $key
                );
            }

            return new Response(HTTP_CREATED, '', ['key' => $key]);
        }

        return new Response(HTTP_NOT_FOUND, 'No such user');
    }

    /**
     * Update resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function update($id = '', $params = [])
    {
        return new Response(HTTP_NOT_FOUND, 'Method is not supported');
    }

    /**
     * Delete resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function delete($id = '', $params = [])
    {
        if (empty($params['key'])) {
            return new Response(HTTP_BAD_REQUEST, 'No API key provided');
        }

        $api_user = Database::getRow(
            'SELECT * FROM api_users WHERE `key` = ?',
            $params['key']
        );

        if (!empty($api_user)) {
            Database::query(
                'DELETE FROM api_users WHERE `key` = ?',
                $params['key']
            );

            return new Response(HTTP_OK, 'Authorization has been deleted');
        }

        return new Response(HTTP_UNAUTHORIZED, 'User is not authorized');
    }

    /**
     * Check if user should be authorized to use this resource
     *
     * @return bool
     */
    public static function shouldBeAuthorized()
    {
        return false;
    }
}
