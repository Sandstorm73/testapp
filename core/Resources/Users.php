<?php

namespace TestApp\Core\Resources;

use TestApp\Core\Interfaces\IResource;
use TestApp\Core\Database;
use TestApp\Core\Response;

defined('BOOTSTRAP') or die('Access denied');

class Users implements IResource
{
    private const ITEMS_PER_PAGE = 5;
    /**
     * Get resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function get($id = '', $params = [])
    {
        $page = (int) $params['page'] ?? 0;
        $page = ($page >= 0) ? $page : 0;

        $total = Database::getField('SELECT COUNT(*) FROM students');

        $users = Database::getArray(
            'SELECT
                user_id,
                username,
                firstname,
                lastname,
                usergroup
            FROM
                students
            LIMIT ?, ?',
            $page * self::ITEMS_PER_PAGE,
            self::ITEMS_PER_PAGE
        );

        return new Response(HTTP_OK, '', [
            'search' => [
                'page' => $page,
                'total' => $total,
            ],
            'users' => $users
        ]);
    }

    /**
     * Create resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function create($id = '', $params = [])
    {
        return new Response(HTTP_NOT_FOUND, 'Method is not supported');
    }

    /**
     * Update resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function update($id = '', $params = [])
    {
        return new Response(HTTP_NOT_FOUND, 'Method is not supported');
    }

    /**
     * Delete resource
     *
     * @param int   $id     Resource entity ID
     * @param array $params Request parameters
     *
     * @return array
     */
    public static function delete($id = '', $params = [])
    {
        return new Response(HTTP_NOT_FOUND, 'Method is not supported');
    }

    /**
     * Check if user should be authorized to use this resource
     *
     * @return bool
     */
    public static function shouldBeAuthorized()
    {
        return true;
    }
}
