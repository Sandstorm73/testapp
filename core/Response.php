<?php

namespace TestApp\Core;

defined('BOOTSTRAP') or die('Access denied');

class Response
{
    private $code = 0;
    private $message = '';
    private $data = null;

    /**
     * Initiates the Response constructor
     *
     * @param int    $code    Response code
     * @param string $message Error message
     * @param array  $data    Response data
     */
    public function __construct($code, $message, $data = null)
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * Returns a response code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Returns a response message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Returns a response data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
