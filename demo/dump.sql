CREATE DATABASE test_app CHARACTER SET utf8;

USE test_app;

CREATE TABLE api_users (
    `key` varchar(64) NOT NULL default '',
    `user_id` int unsigned NOT NULL default 0,
    PRIMARY KEY (`key`)
);

CREATE TABLE students (
    `user_id` int unsigned NOT NULL auto_increment,
    `username` varchar(64) NOT NULL default '',
    `password` varchar(64) NOT NULL default '',
    `usergroup` varchar(64) NOT NULL default '',
    `firstname` varchar(64) NOT NULL default '',
    `lastname` varchar(64) NOT NULL default '',
    PRIMARY KEY (`user_id`)
);


INSERT INTO students (`username`, `password`, `usergroup`, `firstname`, `lastname`) VALUES
    ('sandstorm73', '0124e17cf672f1fc27ce23865f2a92fb94c34088b589b88a55c84b2ac83702c6', 'IT', 'Andrew', 'Ovchenkov'),
    ('black_bird', '1bfc98fbfc2b2af345ee22919a405846c21ba340dfb2297917b5330a5ad37421', 'IT', 'John', 'Doe'),
    ('xxxPredatorxxx', '54a93e2a95c9fbfaae159ccf3647b8cff49cc49c18eae05c2ba16e3346717972', 'IT', 'Sam', 'Smith'),
    ('b3auty', '3388177e65a1fdcf31408c87fd54038802f662dc1d248b20d5804466d028922f', 'Finance', 'Joan', 'Riley'),
    ('NME', '9773b3059a3f2d30883ba28921a35fcd9cf19e5c05f379ae2876c15ad2d10a36', 'Audio', 'Lasley', 'Broderick'),
    ('Hsyfe2212', '8ec59481444c36717585184460e5cb2a1a8f4c6a5c480658d5e3fcd258d4e533', 'Audio', 'Daron', 'Malakian'),
    ('BlOoDyScReAmeR', 'c17b38e609e253be4054eb641362634be154bf25ac900b209f29295bc48a55ac', 'Technical support', 'Tina', 'Farell'),ADD('sandstorm73', '0124e17cf672f1fc27ce23865f2a92fb94c34088b589b88a55c84b2ac83702c6', 'IT', 'Andrew', 'Ovchenkov'),
    ('fake_black_bird', '1bfc98fbfc2b2af345ee22919a405846c21ba340dfb2297917b5330a5ad37421', 'IT', 'John', 'Doe'),
    ('fake_xxxPredatorxxx', '54a93e2a95c9fbfaae159ccf3647b8cff49cc49c18eae05c2ba16e3346717972', 'IT', 'Sam', 'Smith'),
    ('fake_b3auty', '3388177e65a1fdcf31408c87fd54038802f662dc1d248b20d5804466d028922f', 'Finance', 'Joan', 'Riley'),
    ('fake_NME', '9773b3059a3f2d30883ba28921a35fcd9cf19e5c05f379ae2876c15ad2d10a36', 'Audio', 'Lasley', 'Broderick'),
    ('fake_Hsyfe2212', '8ec59481444c36717585184460e5cb2a1a8f4c6a5c480658d5e3fcd258d4e533', 'Audio', 'Daron', 'Malakian'),
    ('fake_BlOoDyScReAmeR', 'c17b38e609e253be4054eb641362634be154bf25ac900b209f29295bc48a55ac', 'Technical support', 'Tina', 'Farell')
;
