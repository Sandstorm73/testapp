var application = function ()
{
    var self = null;
    var page = 0;

    this.init = function () {
        self = this;

        self.key = $.cookie('api_key');
        var page = window.sessionStorage.getItem('page');

        if (!self.key) {
            self.key = window.sessionStorage.getItem('api_key');
        }

        if (!self.key) {
            this.showLoginPage();
        } else {
            this.showUsersPage(page);
        }
    };

    this.showLoginPage = function () {
        $('#username').val('');
        $('#password').val('');
        $('#remember_me').prop('checked', false);
        $('#users_list').addClass('hidden');
        $('#login_form').removeClass('hidden');
        $('#users_container').html('');
    };

    this.showUsersPage = function (page) {


        if (!self.key) {
            self.showLoginPage();
            return;
        }

        $.ajax({
            url: "/api/users",
            method: "GET",
            headers: {
                "Authorization": "Bearer " + self.key
            },
            data: {
                "page": page
            },
            dataType: "json",
            success: function( response ) {
                $('#users_container').html('');

                for (index in response.users) {
                    var content = $('#default_user_container').html();

                    content = content.replace('[username]', response.users[index].username);
                    content = content.replace('[firstname]', response.users[index].firstname);
                    content = content.replace('[lastname]', response.users[index].lastname);
                    content = content.replace('[usergroup]', response.users[index].usergroup);

                    $('#users_container').append(content);
                }

                $('#pagination').html('');
                pagesCount = Math.ceil(response.search.total / 5);
                for (let pageNum = 1; pageNum <= pagesCount; pageNum++) {
                    var pageContent = '';
                    if (page == (pageNum - 1)) {
                        pageContent = '<span class="page selected">' + pageNum + '</span>';
                    } else {
                        pageContent = '<span class="page" onclick="app.showUsersPage(' + (pageNum - 1) + ')">' + pageNum + '</span>';
                    }

                    $('#pagination').append(pageContent);
                }

                $('#users_list').removeClass('hidden');
                $('#login_form').addClass('hidden');
                window.sessionStorage.setItem('page', page);
            },
            error: function( jqXHR, textStatus, errorMessage ) {
                console.log(textStatus);

                self.showLoginPage();
            }
        });
    };


    this.login = function () {
        var username = $('#username').val();
        var password = $('#password').val();
        self.rememberMe = $('#remember_me').prop('checked');

        $.ajax({
            url: "/api/auth",
            method: "POST",
            processData: false,
            data: JSON.stringify({"username": username, "password": password}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function( response ) {
                self.key = response.key;
                window.sessionStorage.setItem('api_key', self.key);

                if (self.rememberMe) {
                    $.cookie('api_key', self.key, {"expires": 14});
                }
                self.showUsersPage(0);
            },
            error: function( jqXHR, textStatus, errorMessage ) {
                self.showLoginPage();
            }
        });
    };

    this.logOut = function () {

        $.ajax({
            url: "/api/auth",
            method: "DELETE",
            processData: false,
            data: JSON.stringify({"key": self.key}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function( response ) {
                self.key = null;
                $.cookie('api_key', null);
                window.sessionStorage.removeItem('api_key');
                self.showLoginPage();
            },
            error: function( jqXHR, textStatus, errorMessage ) {
                self.key = null;
                $.cookie('api_key', null);
                window.sessionStorage.removeItem('api_key');
                self.showLoginPage();
            }
        });
    };
}
